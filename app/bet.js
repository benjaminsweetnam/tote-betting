// Variables
module.exports = function placeBet(bet, type, horse){
var finished = (bet[0] != "Bet" && bet[0] == "Result")
  // Check for finish conndition or wrong bet type
  if(bet[1] == type && !finished){
    // Checks if Horse has a Place Bet
    if(!horse[(bet[2]-1)]){
      // Assigns Bet to Horse if no other value is present
      horse[(bet[2]-1)] = parseInt(bet[3])
    } else {
      // Adds to Horse Bet Value if Already Present
      horse[(bet[2]-1)] += parseInt(bet[3])
    }
    // If finish conndition has been met ie bet[0] == "Result" return PlaceBets
  } else if(finished){
    return horse
  }
}
