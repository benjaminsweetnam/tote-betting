module.exports = function calcResults(win, place, results, winPool, placePool){
  // remove /n from results entry
  results[3] = parseInt(results[3])
  // Win Payout Calculation
  winPayout = winPool * 0.85
  winBet = win[(results[1]-1)]
  winShare = Math.round((winPayout/winBet)*100)/100

  // First Place Payout Calculation
  placePayout = (placePool * 0.88)/3
  firstPlaceBet = place[(results[1]-1)]
  firstPlaceShare = Math.round((placePayout/firstPlaceBet)*100)/100

  // Second Place Payout Calculation
  secondPlaceBet = place[(results[2]-1)]
  secondPlaceShare = Math.round((placePayout/secondPlaceBet)*100)/100

  // Third Place Payout Calculation
  thirdPlaceBet = place[(results[3]-1)]
  thirdPlaceShare = Math.round((placePayout/thirdPlaceBet)*100)/100

  // Output Generator
  var output = "Win:" + results[1] +':$' + winShare + '\n'
  output += "Place:" + results[1] + ':$' + firstPlaceShare + '\n'
  output += "Place:" + results[2] + ':$' + secondPlaceShare + '\n'
  output += "Place:" + results[3] + ':$' + thirdPlaceShare + '\n'
  return output
}


// total winPool should = 338
// payout 287.3
// winPayed 110
// output 2.61

// total placePool = 646
// payout = 568.48
// shared placePool = 189.49333
// first place = 179 => 1.05862197
