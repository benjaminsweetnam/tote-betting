process.stdin.setEncoding('utf8')
// Imports
var Bet = require('./bet')
var calcRes = require('./calcResults')
// Declarations
var winHorse = []
var winPool = 0
var placeHorse = []
var placePool = 0
var result = []

process.stdin.on('readable', () => {
  chunk = process.stdin.read()
  if(chunk != null){
    chunk = chunk.split(":")
    // accepts a bet as an Array and returns an array of Win Bets matched to each horse
    var win = Bet(chunk, 'W', winHorse)
    if(chunk[1] == 'W'){
      // counts total Win Bets
      winPool += parseInt(chunk[3])
    }
    // accepts a bet as an Array and returns an array of Place Bets matched to each horse
    var place = Bet(chunk, 'P', placeHorse)
    if(chunk[1] == 'P'){
      // counts total Place Bets
      placePool += parseInt(chunk[3])
    }
    // Checks wether result has been returned
    var finished = (chunk[0] == "Result")
    if(finished){
      // sends all information to calcResults.js to calculate Results
      var results = calcRes(win, place, chunk, winPool, placePool)
      process.stdout.write(`\n${results}`);
    }
  }
})

// process.stdin.on('end', () => {
//   process.stdout.write('end')
//   process.exit()
// })
